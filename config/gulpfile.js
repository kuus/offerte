const gulp = require('gulp');
const scripts = require('./dev-lib/gulp/scripts');
const scriptsVendor = require('./dev-lib/gulp/scriptsVendor');
const styles = require('./dev-lib/gulp/styles');
const injectStyles = require('./dev-lib/gulp/injectStyles');
const injectScripts = require('./dev-lib/gulp/injectScripts');
const views = require('./dev-lib/gulp/views');
const images = require('./dev-lib/gulp/images');
const fonts = require('./dev-lib/gulp/fonts');
const extras = require('./dev-lib/gulp/extras');
const info = require('./dev-lib/gulp/info');
const optimize = require('./dev-lib/gulp/optimize');
const clean = require('./dev-lib/gulp/clean');
const modernizr = require('./dev-lib/gulp/modernizr');
const deployGithub = require('./dev-lib/gulp/deployGithub');
const serveDist = require('./dev-lib/gulp/serveDist');
const html = require('./dev-lib/gulp/html');
const watch = require('./dev-lib/gulp/watch');

// Public tasks
gulp.task('inject', gulp.parallel(injectStyles, injectScripts));
gulp.task('serve', gulp.series('inject', gulp.parallel(views, styles, scripts, modernizr, fonts), watch));
gulp.task('build', gulp.series(clean, 'inject', scriptsVendor, gulp.parallel(views, styles, scripts, modernizr, fonts, images, extras), html, optimize, info));
gulp.task('build').description = 'an example of build task: `$ gulp build --dist mangle,htmlmin,static,inline`';
gulp.task('default', gulp.task('serve'));
gulp.task(serveDist);
gulp.task('deploy', deployGithub);
gulp.task(info);

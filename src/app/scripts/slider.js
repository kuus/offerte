import $ from 'jquery';
import api from './api';

class Slider {

  /**
   * Constructor
   *
   * @param  {jQuery} container
   * @param  {?Object} mainOptions
   * @param  {?Object} thumbsOption
   */
  constructor (container, mainOptions, thumbsOption) {
    this.__$container = container;
    this.__$main = this.__$container.find('.appSlider__main');
    this.__$thumbs = this.__$container.find('.appSlider__thumbs');
    this.__$next = this.__$container.find('.appSlider__next');
    this.__$prev = this.__$container.find('.appSlider__prev');
    this.__$counter = this.__$container.find('.appSlider__counter');

    this._setMainOpts(mainOptions);
    this._setThumbsOpts(thumbsOption);

    this.init();
  }

  /**
   * Set main slider options
   *
   * @param  {?Object} custom
   */
  _setMainOpts (custom) {
    if (this.__$main.length) {
      this._mainOpts = $.extend(true, {
        dots: false,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
      }, custom);
    }

    if (this.__$thumbs.length) {
      this._mainOpts.asNavFor = this.__$thumbs;
    } else {
      // if there are no thumbs use the arrows for the main slider
      // if they are in the markup
      if (this.__$next.length) {
        this._mainOpts.arrows = true;
        this._mainOpts.nextArrow = this.__$next;
      }
      if (this.__$prev.length) {
        this._mainOpts.arrows = true;
        this._mainOpts.prevArrow = this.__$prev;
      }
    }
  }

  /**
   * Set thumbnails slider options
   *
   * @param  {?Object} custom
   */
  _setThumbsOpts (custom) {
    if (this.__$thumbs.length) {
      this._thumbsOpts = $.extend(true, {
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        slidesToShow: 3.5,
        slidesToScroll: 3.5,
        // infinite: false,
        // infinite: true,
        // initialSlide: 1,
        prevArrow: this.__$prev,
        nextArrow: this.__$next,
        asNavFor: this.__$main,
      }, custom);
    }
  }

  /**
   * Init
   *
   * @@ref $screen-xs-min: 480px
   * @@ref $screen-sm-min: 768px
   * @@ref $screen-md-min: 992px
   * @@ref $screen-lg-min: 1200px
   */
  init () {
    let sliderForCounter = null;

    // init main slider
    if (this.__$main.length) {
      sliderForCounter = this.__$main;
      this.__$main.slick(this._mainOpts);
    }

    // init thumbs slider
    if (this.__$thumbs.length) {
      sliderForCounter = this.__$thumbs;
      this.__$thumbs.slick(this._thumbsOpts);
    }

    // init counter
    if (this.__$counter.length) {
      this._slick = sliderForCounter.slick('getSlick');
      this._updateCounter();
      sliderForCounter.on('afterChange', this._updateCounter.bind(this));
    }

    // this._$win.resize(this._onResize.bind(this));
  }

  /**
   * UpdateCounter
   */
  _updateCounter () {
    const slideCount = this._slick['slideCount'];
    const slideCurrent = this._slick['currentSlide'];

    this.__$counter.text(`${slideCurrent + 1}/${slideCount}`);
  }
}

// export to public API
api['Slider'] = Slider;

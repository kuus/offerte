// allow this to be set from outside of this script, e.g. from the backend
const api = window['offerte'] || {};

export default window['offerte'] = api;

import $ from 'jquery';
import 'bootstrap-sass/assets/javascripts/bootstrap/collapse';
import 'bootstrap-sass/assets/javascripts/bootstrap/dropdown';
import 'bootstrap-sass/assets/javascripts/bootstrap/transition';
// import 'headroom.js/dist/headroom.js';
// import 'headroom.js/dist/jQuery.headroom.js';
// import 'jquery-match-height/dist/jquery.matchHeight';
// import 'slick-carousel/slick/slick.js';

import api from './api';
import './slider';

class App {

  constructor () {
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    // console.log('app.js initialized');
    this._updateCopyrightYear();
  }

  /**
   * On document ready
   */
  $onReady () {
    this._$win = $(window);
    this._matchColsHeight();
    this._initHeader();
    this._initForms();
    this._initSliders();

    this._$win.resize(this._matchColsHeight.bind(this));
  }

  /**
   * Match the height of the item cards' columns
   */
  _matchColsHeight () {
    $('.colMatchHeight').matchHeight({});
  }

  /**
   * Init header behaviour
   */
  _initHeader () {
    $('.appHeader').headroom({
      offset: window.innerWidth > 480 ? 100 : 30,
      tolerance: 2
    });
  }

  /**
   * Init forms (validation), compatible with Bootstrap v3
   *
   * @see https://stackoverflow.com/a/18754780/1938970
   */
  _initForms () {
    $('.form-validate').validate({
      errorClass: 'has-feedback has-error',
      validClass: 'has-feedback has-success',
      errorElement: 'span',
      errorClass: 'help-feedback',
      errorPlacement: function(error, element) {
        const $parent = element.parent();
        if ($parent.hasClass('input-group')) {
          error.insertAfter($parent);
        }
        else if ($parent.hasClass('custom-control')) {
          $parent.after(error);
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element) {
        $(element).parents('.form-group').addClass('has-feedback has-error')
          .removeClass('has-warning has-success');
      },
      unhighlight: function(element) {
        $(element).parents('.form-group').addClass('has-feedback has-success')
          .removeClass('has-error has-warning');
      }
    });
  }

  /**
   * Init sliders
   *
   * @@ref $screen-xs-min: 480px
   * @@ref $screen-sm-min: 768px
   * @@ref $screen-md-min: 992px
   * @@ref $screen-lg-min: 1200px
   */
  _initSliders () {
    const $sliderReviews = $('.appReview__slider');
    if ($sliderReviews.length) {
      this.sliderReviews = new api.Slider($sliderReviews, {
        infinite: false,
        centerMode: true,
        initialSlide: 2,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        }, {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
          }
        }]
      });
    }

    const $sliderPartners = $('.appPartners__slider');
    if ($sliderPartners.length) {
      this.sliderPartners = new api.Slider($sliderPartners, {
        infinite: false,
        slidesToShow: 6,
        slidesToScroll: 6,
        responsive: [{
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
          }
        }]
      });
    }

    const $sliderCompanyReviews = $('.appCompany__signup-reviewsslider');
    if ($sliderCompanyReviews.length) {
      this.sliderCompanyReviews = new api.Slider($sliderCompanyReviews, {
        infinite: false,
        centerMode: true,
        variableWidth: true,
        initialSlide: 2,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 480,
          settings: {
            variableWidth: false,
            slidesToShow: 1,
          }
        }]
      });
    }

    const $sliderProfile = $('#appProfile__slider');
    if ($sliderProfile.length) {
      this.sliderProfile = new api.Slider($sliderProfile);
    }
  }

  /**
   * Update copyright year
   */
  _updateCopyrightYear() {
    var el = document.getElementById('js-copyyear');
    if (el) {
      el.innerHTML = new Date().getFullYear();
    }
  }
}

// export to public API
api['app'] = new App();
